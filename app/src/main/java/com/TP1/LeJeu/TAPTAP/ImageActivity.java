package com.TP1.LeJeu.TAPTAP;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class ImageActivity extends AppCompatActivity {

    private TextView tvScore;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.image_main);
        tvScore = findViewById(R.id.tv_scoreB);
        Intent intent = getIntent();
        long score = intent.getLongExtra("score", 0);
        tvScore.setText(String.valueOf(score));
    }
}
