package com.TP1.LeJeu.TAPTAP;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btConnextion;
    private EditText etNom, etPassword;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btConnextion = findViewById((R.id.bt_connexion));
        etNom = findViewById(R.id.et_Nom);
        etPassword = findViewById(R.id.et_Password);

        btConnextion.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if (etNom.getText().toString().equals("bot") && etPassword.getText().toString().equals("123")){
            //Ouverture de la second activity
            Intent intent = new Intent(MainActivity.this, JeuActivity.class);
            intent.putExtra("pseudo", etNom.getText().toString());
            startActivity(intent);

        } else {
            // toast annonce le mauvais mot de passe.
            Toast.makeText(this, "Mot de passe ou pseudo invalide.", Toast.LENGTH_SHORT).show();
        }
    }
}