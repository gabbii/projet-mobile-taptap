package com.TP1.LeJeu.TAPTAP;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class JeuActivity extends AppCompatActivity implements View.OnClickListener {
    private final int REQUEST_CODE = 1234;
    private TextView tvPseudo;
    private Button btSetting;
    private TextView tvScore;
    private Button btJeu1;
    private Button btJeu2;
    private Button btJeu3;
    private Button btJeu4;
    private Button btGo;

    boolean bt1click = true;
    boolean bt2click = false;
    boolean bt3click = false;
    boolean bt4click = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.jeu_main);
        Intent intent;
        tvPseudo = findViewById(R.id.tv_pseudo);
        btSetting = findViewById(R.id.bt_settings);
        tvScore = findViewById(R.id.tv_score);
        btJeu2 = findViewById(R.id.bt_Jeu2);
        btJeu3 = findViewById(R.id.bt_Jeu3);
        btJeu1 = findViewById(R.id.bt_Jeu1);
        btJeu4 = findViewById(R.id.bt_Jeu4);
        btGo = findViewById(R.id.bt_go);

        //attribuer le bon pseudo.
        intent = getIntent();
        // Si on vient juste de se connecter.
        if (intent.hasExtra("pseudo")) {
            String pseudoa = intent.getStringExtra("pseudo");
            tvPseudo.setText(pseudoa);
        }else{
            //sinon, on prend la valeur des sharedprefs
            SharedPreferences sp = getSharedPreferences("MySharedPref", MODE_PRIVATE);
            String pseudoa = sp.getString("pseudo","");
            tvPseudo.setText(pseudoa);

        }

        //affichage de la page des setting
        btSetting.setOnClickListener(view -> {
            Intent intent2 = new Intent(JeuActivity.this, SettingActivity.class);
            intent2.putExtra("pseudo", tvPseudo.getText().toString());
            startActivityForResult(intent2, REQUEST_CODE);
        });
        // on donne la valeur au score.
        SharedPreferences sp = getSharedPreferences("MySharedPref", MODE_PRIVATE);
        long scorelong = sp.getLong("scoreLong", 0);
        int score = sp.getInt("score", 0);
        // Si il y avait une valeur au score différente de 0
        if(scorelong !=0){
            tvScore.setText(String.valueOf(scorelong));
        }else{
            //sinon, on va chercher dans score
            tvScore.setText(String.valueOf(score));
        }




        btJeu1.setOnClickListener(this);
        btJeu2.setOnClickListener(this);
        btJeu3.setOnClickListener(this);
        btJeu4.setOnClickListener(this);
        btGo.setOnClickListener(this);

    }


    //retour à la page avec le nouveau pseudo.
    protected void onActivityResult(int req, int res, Intent data) {
        if (req == REQUEST_CODE) {
            if (res == RESULT_OK) {
                String message = data.getStringExtra(SettingActivity.MESSAGE);
                // si aucun speudo
                if (message != ""){
                    tvPseudo.setText(message);
                }

            }
            // Si on revient avec le resetScore, on enlève la valeur du score.
            SharedPreferences sp = getSharedPreferences("MySharedPref", MODE_PRIVATE);
            sp.edit().remove("scoreLong").commit();
            tvScore.setText(String.valueOf(0));
        }

    }

    //
    @Override
    public void onClick(View v) {
        long temps1;
        switch(v.getId()){

            case R.id.bt_Jeu1:
                if (bt1click) {
                    bt1click = false;
                    btGo.setVisibility(View.VISIBLE);
                    btJeu1.setAlpha((float) 0.6);
                    btJeu1.setText("Go!");
                    bt2click = true;
                    temps1 = System.currentTimeMillis();
                    // on met le temps actuel dans les préférences.
                    SharedPreferences sharedPreferences = getSharedPreferences("MySharedPref",MODE_PRIVATE);
                    SharedPreferences.Editor myEdit2 = sharedPreferences.edit();
                    myEdit2.putLong("temps1", temps1);
                    myEdit2.commit();
                }
                break;

            case R.id.bt_Jeu2:
                if (bt2click){
                    bt2click = false;
                    btJeu2.setAlpha((float) 0.6);
                    long tempsactuel2 = System.currentTimeMillis();
                    SharedPreferences sharedPreferences = getSharedPreferences("MySharedPref", MODE_PRIVATE);
                    temps1 = sharedPreferences.getLong("temps1", 0);
                    long tempsclick2 = tempsactuel2 - temps1;
                    btJeu2.setText(String.valueOf(tempsclick2));
                    bt3click = true;
                }

                break;

            case R.id.bt_Jeu3:
                if(bt3click) {
                    bt3click = false;
                    btJeu3.setAlpha((float) 0.6);
                    long tempsactuel2 = System.currentTimeMillis();
                    SharedPreferences sharedPreferences = getSharedPreferences("MySharedPref", MODE_PRIVATE);
                    temps1 = sharedPreferences.getLong("temps1", 0);
                    long tempsclick3 = tempsactuel2 - temps1;
                    btJeu3.setText(String.valueOf(tempsclick3));
                    bt4click = true;

                }
                break;
            case R.id.bt_Jeu4:
                if(bt4click){
                    bt4click = false;
                    btJeu4.setAlpha((float) 0.6);
                    long tempsactuel4 = System.currentTimeMillis();
                    SharedPreferences sharedPreferences = getSharedPreferences("MySharedPref", MODE_PRIVATE);
                    temps1 = sharedPreferences.getLong("temps1", 0);
                    long tempsclick4 = tempsactuel4 - temps1;
                    btJeu4.setText(String.valueOf(tempsclick4));
                    //actualisation du score
                    if (tempsclick4 < Long.parseLong(tvScore.getText().toString()) && tempsclick4 != 0 ){

                        tvScore.setText(String.valueOf(tempsclick4));
                        //met le score dans les préférence
                        SharedPreferences sharedPreferences2 = getSharedPreferences("MySharedPref",MODE_PRIVATE);
                        SharedPreferences.Editor myEdit2 = sharedPreferences2.edit();
                        myEdit2.putLong("scoreLong", tempsclick4);
                        myEdit2.commit();
                    }
                    //ouvertrure de la page image
                    if(tempsclick4 >= 1000 && tempsclick4 <= 2000){
                        Intent intent = new Intent(JeuActivity.this, ImageActivity.class);
                        intent.putExtra("score", tempsclick4);
                        startActivity(intent);
                    }

                }
                break;
            case R.id.bt_go:
                // au click du boutton go, on recommence le jeu.
                btJeu1.setAlpha((float) 1);
                btJeu2.setAlpha((float) 1);
                btJeu3.setAlpha((float) 1);
                btJeu4.setAlpha((float) 1);
                bt1click = true;
                bt2click = false;
                bt3click = false;
                bt4click = false;
                btJeu1.setText("1");
                btJeu2.setText("2");
                btJeu3.setText("3");
                btJeu4.setText("4");
                break;



        }
    }
}

