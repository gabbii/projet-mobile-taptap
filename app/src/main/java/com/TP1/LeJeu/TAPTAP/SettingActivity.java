package com.TP1.LeJeu.TAPTAP;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class SettingActivity extends AppCompatActivity {
    public static final String MESSAGE = "MESSAGE";
    private Button btOk;
    private EditText etPseudo;
    private Button btResetScore;
    private Button btEnvoyer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.setting_main);

        btOk = findViewById(R.id.bt_okchange);
        etPseudo = findViewById(R.id.et_changepseudo);
        btResetScore = findViewById(R.id.bt_reset);
        btEnvoyer = findViewById(R.id.bt_envoyer);

        // On met le pseudo dans le edit text.
        Intent intent = getIntent();
        String pseudo = (intent.getStringExtra("pseudo"));
        etPseudo.setText(pseudo);

        // on met le pseudo dans les shared preferences pour le ramener tel quel à la page Jeu si on utilise
        // la flèche pour le retour au parent.
        SharedPreferences sharedPreferences = getSharedPreferences("MySharedPref",MODE_PRIVATE);
        SharedPreferences.Editor myEdit = sharedPreferences.edit();
        myEdit.putString("pseudo",etPseudo.getText().toString());
        myEdit.commit();

        //reset du score
        btResetScore.setOnClickListener(view -> {
            SharedPreferences sharedPreferences2 = getSharedPreferences("MySharedPref",MODE_PRIVATE);
            SharedPreferences.Editor myEdit2 = sharedPreferences2.edit();
            myEdit2.putInt("score", 0);
            myEdit2.commit();
            finish();
        });

        //changement du pseudo au click du ok
        btOk.setOnClickListener(view -> {
            String changementPseudo = etPseudo.getText().toString();
            Intent intent2 = new Intent();
            intent2.putExtra(MESSAGE, changementPseudo);
            setResult(RESULT_OK, intent2);
            finish();
        });

        btEnvoyer.setOnClickListener(view->{
            SharedPreferences sharedPref = getSharedPreferences("MySharedPref", MODE_PRIVATE);
            long scoreLong = sharedPref.getLong("scoreLong", 0);
            Intent shareIntent = new Intent();
            shareIntent.setAction(Intent.ACTION_SEND);
            shareIntent.putExtra(Intent.EXTRA_TEXT, "Mon score :" + scoreLong);
            shareIntent.setType("text/plain");
            startActivity(Intent.createChooser(shareIntent, "Share"));
        });


    }
}